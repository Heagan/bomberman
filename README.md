# Slime Bomberman

Built a custom bomberman game in a team of 3 for our collage final project.
Coded in C/C++.

The project needed us to build as much as possible on our own. 
We had to write the maths and logic needed render 3D objects, lighting, animations, sound and menu's of the game.

## Features
It has the core functionalities of the original bomberman game. 
Where the goal is to kill all enemies on the map
and then find the hidden door to the next level.

Player has 3 lives and a timer to complete each level.
The game includes:
- 3 Maps
- 2 Powerups

# Preview
![](bomberman.png)

# Build and run
There is a Cmake file provided, should compile for Unix, Mac and Windows

## Windows
Needs specific .dll files which can be hard to find
so they are included in a seperate branch called `dependencies`,
just copy them to the games .exe directory


