//
// Created by Leonard VAN GEND on 2018/09/25.
//

#ifndef BOMBERBOI_DYNAMICTEXT_HPP
#define BOMBERBOI_DYNAMICTEXT_HPP


#include <string>
#include <Includes/TextModel.hpp>
#include <Includes/TextDisplaySystem.hpp>

class GameText {
public:
	GameText(TextDisplaySystem *textDisplaySystem, std::string startingText, glm::vec3 color);
	GameText();
	GameText(const GameText &obj);
	GameText& operator=(const GameText &obj);
	~GameText();

	void draw(Shader &shader);
	void update(std::string &newValue);
	void translate(glm::vec3 translation);
	void rotate(glm::vec3 rotation);
	void scale(glm::vec3 scale);

private:
	std::string			m_currentValue;
	TextModel			*m_modelPtr;
	TextDisplaySystem	*m_textDisplaySystem;
	glm::vec3			m_color;
	Transformation		m_currentTransformation;
};


#endif //BOMBERBOI_DYNAMICTEXT_HPP
