//
// Created by Leonard VAN GEND on 2018/09/25.
//

#include <iostream>
#include <glad/glad.h>
#include <openGLFunctionCallErrorManagementWrapper.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include "InGamePlayTextDisplay.hpp"

InGamePlayTextDisplay::InGamePlayTextDisplay() {

}

InGamePlayTextDisplay::InGamePlayTextDisplay(const InGamePlayTextDisplay &obj) {
	*this = obj;
	std::cout << "ERROR: InGamePlayTextDisplay cannot be coppied" << std::endl;
	assert(false);
}

InGamePlayTextDisplay &InGamePlayTextDisplay::operator=(const InGamePlayTextDisplay &obj) {
	this->m_windowHeight = obj.m_windowWidth;
	std::cout << "ERROR: InGamePlayTextDisplay cannot be coppied" << std::endl;
	assert(false);
	return *this;
}

InGamePlayTextDisplay::~InGamePlayTextDisplay() {
	delete this->m_textDisplaySystem;
}

InGamePlayTextDisplay::InGamePlayTextDisplay(int windowWidth, int windowHeight, std::string fontFile, int textSize) {
	this->m_fontFile = fontFile;
	this->m_textSize = textSize;
	this->m_windowHeight = windowHeight;
	this->m_windowWidth = windowWidth;
	this->m_textDisplaySystem = new TextDisplaySystem(this->m_fontFile, this->m_textSize);
	glm::mat4 projection = glm::ortho(0.0f, (float)windowWidth, 0.0f, (float)windowHeight);
	this->m_shader = &m_textDisplaySystem->getTextShader();
	this->m_shader->bind();
	this->m_shader->setUniformMatrix4fv("projection", glm::value_ptr(projection));
	this->m_shader->unbind();
	this->m_playerCharacter = Player::getInstance();
}

void InGamePlayTextDisplay::drawText() {
	GL_ERROR_WRAPPER(glDisable(GL_DEPTH_TEST));
	GL_ERROR_WRAPPER(glEnable(GL_BLEND));
	GL_ERROR_WRAPPER(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	this->m_shader->bind();
	for (GameText &updatableText: this->m_updatableTextList) {
		updatableText.draw(*this->m_shader);
	}
	for (GameText &nonUpdatableText: this->m_nonupdatableTextList)
		nonUpdatableText.draw(*m_shader);
	this->m_shader->unbind();
	GL_ERROR_WRAPPER(glEnable(GL_DEPTH_TEST));
	GL_ERROR_WRAPPER(glDisable(GL_BLEND));
}

void InGamePlayTextDisplay::updateAllDynamicText() {
	std::vector<int> newValues = {this->m_playerCharacter->getLives(),
							   (int)this->m_playerCharacter->getBombRadius(),
								  this->m_playerCharacter->getScore(),
								  (int)this->m_currentTime};

	int textId = 0;
	for (GameText &dynamicText : this->m_updatableTextList) {
		std::string valueString = std::to_string(newValues[textId]);
		dynamicText.update(valueString);
		textId++;
	}
}

void InGamePlayTextDisplay::update() {
	this->updateAllDynamicText();
	this->drawText();
}

void InGamePlayTextDisplay::resetClock() {
	this->m_lastResetClockCallTimeInMilliseconds = SDL_GetTicks();
}

void InGamePlayTextDisplay::resetUpdatableValues() {
	this->resetClock();

	std::vector<int> currentValueList;
	currentValueList.push_back(this->m_playerCharacter->getLives());
	currentValueList.push_back((int)this->m_playerCharacter->getBombRadius());
	currentValueList.push_back(this->m_playerCharacter->getScore());
	currentValueList.push_back(this->m_currentTime);

	GameText startLives(this->m_textDisplaySystem, std::to_string(currentValueList[0]), {1, 0.6, 0.7});
	GameText startBombRange(this->m_textDisplaySystem, std::to_string(currentValueList[1]), {1, 0, 0});
	GameText startScore(this->m_textDisplaySystem, std::to_string(currentValueList[2]), {0, 0, 1});
	GameText startTime(this->m_textDisplaySystem, std::to_string(currentValueList[3]), {0, 1, 0});

	startLives.translate(    glm::vec3(150, this->m_windowHeight - 50, 0));
	startBombRange.translate(glm::vec3(400, this->m_windowHeight - 50, 0));
	startScore.translate(    glm::vec3(650, this->m_windowHeight - 50, 0));
	startTime.translate(     glm::vec3(1025, this->m_windowHeight - 50, 0));

	std::vector<GameText> updatableValueModelList;
	updatableValueModelList.push_back(startLives);
	updatableValueModelList.push_back(startBombRange);
	updatableValueModelList.push_back(startScore);
	updatableValueModelList.push_back(startTime);

	this->m_updatableTextList = updatableValueModelList;
}

void InGamePlayTextDisplay::addNewText(std::string text, glm::vec3 location, glm::vec3 color) {
	location.y = this->m_windowHeight - location.y;
	this->m_nonupdatableTextList.push_back(GameText(this->m_textDisplaySystem, text, color));
	this->m_nonupdatableTextList.back().translate(location);
}

void InGamePlayTextDisplay::clearText() {
	this->m_nonupdatableTextList.clear();
	this->m_updatableTextList.clear();
	this->m_textDisplaySystem->deleteStoredTextModelMeshs();
}

void InGamePlayTextDisplay::setCurrentTime(unsigned long currentTime) {
	this->m_currentTime = currentTime;
}
