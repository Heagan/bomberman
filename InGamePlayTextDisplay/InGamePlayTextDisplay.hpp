//
// Created by Leonard VAN GEND on 2018/09/25.
//

#ifndef BOMBERBOI_INGAMEPLAYGUI_HPP
#define BOMBERBOI_INGAMEPLAYGUI_HPP


#include <Model.hpp>
#include <Camera.hpp>
#include <Includes/TextDisplaySystem.hpp>
#include <Player.class.hpp>
#include "GameText.hpp"

#define NUM_OF_DISPLAYABLE_VALUES 4;

class InGamePlayTextDisplay {
public:
	InGamePlayTextDisplay();
	InGamePlayTextDisplay(int windowWidth, int windowHeight, std::string fontFile, int textSize);
	InGamePlayTextDisplay(const InGamePlayTextDisplay &obj);
	InGamePlayTextDisplay &operator=(const InGamePlayTextDisplay &obj);
	~InGamePlayTextDisplay();

	void update();
	void resetUpdatableValues();
	void resetClock();
	void addNewText(std::string text, glm::vec3 location, glm::vec3 color);
	void clearText();
	void setCurrentTime(unsigned long);

private:
	void	drawText();
	void	updateAllDynamicText();

	int 						m_textSize;
	std::string					m_fontFile;
	int 						m_windowHeight;
	int 						m_windowWidth;
	unsigned long				m_currentTime;

	std::vector<GameText>	m_updatableTextList;
	std::vector<GameText>	m_nonupdatableTextList;
	Shader						*m_shader;
	TextDisplaySystem			*m_textDisplaySystem;
	unsigned long				m_lastResetClockCallTimeInMilliseconds;
	Player						*m_playerCharacter;
};


#endif //BOMBERBOI_INGAMEPLAYGUI_HPP
