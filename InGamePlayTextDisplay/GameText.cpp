//
// Created by Leonard VAN GEND on 2018/09/25.
//

#include <iostream>
#include "GameText.hpp"

GameText::GameText(TextDisplaySystem *textDisplaySystem, std::string startingText, glm::vec3 color) {
	this->m_textDisplaySystem = textDisplaySystem;
	this->m_currentValue = startingText;
	this->m_modelPtr = textDisplaySystem->getTextModel(this->m_currentValue);
	this->m_color = color;
}

GameText::GameText() {

}

GameText::GameText(const GameText &obj) {
	*this = obj;
}

GameText &GameText::operator=(const GameText &obj) {
	if (this != &obj){
		this->m_currentValue = obj.m_currentValue;
		this->m_currentTransformation = obj.m_currentTransformation;
		this->m_color = obj.m_color;
		this->m_textDisplaySystem = obj.m_textDisplaySystem;
		this->m_modelPtr = this->m_textDisplaySystem->getTextModel(this->m_currentValue);
		this->m_modelPtr->scale(this->m_currentTransformation.m_scaling);
		this->m_modelPtr->translate(this->m_currentTransformation.m_translation);
		this->m_modelPtr->rotate(this->m_currentTransformation.m_rotation);
	}
	return *this;
}

GameText::~GameText() {
	delete this->m_modelPtr;
}

void GameText::draw(Shader &shader) {
	this->m_modelPtr->draw(shader, this->m_color);
}

void GameText::update(std::string &newValue) {
	if (newValue != this->m_currentValue) {
		delete this->m_modelPtr;

		this->m_currentValue = newValue;
		this->m_modelPtr = this->m_textDisplaySystem->getTextModel(newValue);
		this->m_modelPtr->translate(this->m_currentTransformation.m_translation);
		this->m_modelPtr->rotate(this->m_currentTransformation.m_rotation);
		this->m_modelPtr->scale(this->m_currentTransformation.m_scaling);
	}
}

void GameText::translate(glm::vec3 translation) {
	this->m_currentTransformation.m_translation = translation;
	this->m_modelPtr->translate(translation);
}

void GameText::rotate(glm::vec3 rotation) {
	this->m_currentTransformation.m_rotation = rotation;
	this->m_modelPtr->rotate(rotation);
}

void GameText::scale(glm::vec3 scale) {
	this->m_currentTransformation.m_scaling = scale;
	this->m_modelPtr->scale(scale);
}
