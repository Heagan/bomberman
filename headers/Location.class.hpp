#ifndef LOCATION_H
# define LOCATION_H

class Location {

public:
	Location( int x = 0, int y = 0, int z = 0 );
	Location( Location &object );
	~Location( void );
	Location	&operator=( Location const &object );
	Location	operator-( Location const &object );
	bool		operator==( Location const &object );

	Location	&append(int x, int y);

	int	x;
	int	y;
	int	z;

};

#endif
