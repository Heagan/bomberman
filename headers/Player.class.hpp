#ifndef PLAYER_H
# define PLAYER_H

# include "Actor.class.hpp"
# include "Sound.hpp"

class Player : public Actor {

public:
	static Player		*getInstance( void );
	static Player		*player;

	int					getLives( void ) { return _lives; };
	int					getScore( void ) { return _score; };
	unsigned int		getBombRadius( void ) { return _bombRadius; };

	void 				setLives(int amount);

	void	takeDamage( int damage = 1 );
	void	recoverLives( int amount = 1 );
	
	void	increaseBombRadius( unsigned int amount = 1);
	void 	increaseScore(unsigned int amount);

	void 	reinit();

	int 	playerIndex;

private:
	Player( int lives = 3, int score = 0 );
	Player( Player &object );
	~Player( void );
	Player &operator=( Player const &object );

	int				_lives;
	int				_score;
	Uint32			_bombRadius;
	Uint32 			_damageDelayInMilliseconds;
	Sound  					m_die_sound;
	
};

#endif
