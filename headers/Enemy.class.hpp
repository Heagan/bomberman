#ifndef ENEMY_H
# define ENEMY_H

# include "Actor.class.hpp"

class Enemy : public Actor {

public:
	Enemy( Location l );
	Enemy( Enemy &object );
	~Enemy( void );
	Enemy &operator=( Enemy const &object );
	
	void		setMoveSpeed( unsigned int moveSpeed );
	Location	move( void );

	Location	m_previousLoc;

	int			m_enemyModelIndex;

private:
	bool		delay( void );

	Uint32		m_moveSpeed;
	Uint32		m_timerInMilliseconds;

};

#endif
