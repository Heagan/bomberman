#ifndef POWERUP_H
# define POWERUP_H

# include "Location.class.hpp"
# include "bomber.h"

class Powerup {

public:
	Powerup( Location location, E_Powerup type = E_None );
	Powerup( Powerup &object );
	~Powerup( void );
	Powerup &operator=( Powerup const &object );

	Location	location;
	E_Powerup	getType() { return type; };

	int 		powerUpIndex;

private:
	E_Powerup	type;

};

#endif
