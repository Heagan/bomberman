#ifndef LEVEL_H
# define LEVEL_H

# include "bomber.h"
# include "Map.class.hpp"

class Level
{

    public:
	    Level( void );
	    Level(char *fname);
	    Level(Level &) = default;
	    Level& operator= (Level &) = default;
        void parseChar(char c, Map *World, Location l);
        Map *createWorld();
	    ~Level();
    private: 
      std::ifstream		file;
      std::stringstream buf;
      std ::string		fileContents;
      size_t			lines;

};
#endif