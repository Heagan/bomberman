#ifndef WALL_H
# define WALL_H

# include	"bomber.h"
# include	"Location.class.hpp"

class Wall {

public:
	Wall( Location location, E_WallType type = E_INVULNERABLE );
	Wall( Wall &objects );
	~Wall( void );
	Wall &operator=( Wall const &object );

	E_WallType	type;
	Location	location;

	int 		m_modelIndex;

};

#endif