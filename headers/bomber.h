#ifndef BOMBER_H
# define BOMBER_H

# define FPS 60.0
# define SEED 3

enum E_ActorType {E_ENEMY, E_PLAYER};
enum E_Effects {};
enum E_WallType { E_INVULNERABLE, E_DESTRUCTIBLE, E_HIDDENDOORWAY};
enum E_Powerup {E_None, E_FireUp, E_OneUp, E_DoorClosed, E_DoorOpen};
enum E_Input{E_Default, E_Left, E_Right, E_Up, E_Down, E_DropBomb, E_Exit};

extern int G_VOLUME;

# include <iostream>
# include <string>
# include <vector>

//KEY BINDING .CPP
# include <map>

# include <cstdlib>
# include <string.h>

// SLEEP_FOR
#include <chrono>
#include <thread>

//Used to thread in Game.class
//# include <future>

// File reading
# include <fstream>
# include <cstdio>

//std::fabs -> m_map explode bomb
# include <cmath>

// Level Class
# include <algorithm>
# include <sstream>

# include <SDL.h>

//DISPLAY
//# include "Display.hpp"
//# include "../i_Display.hpp"

#endif