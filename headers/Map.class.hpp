#ifndef MAP_H
# define MAP_H

# include	<Includes/Music.hpp>
# include	"bomber.h"
# include	"Location.class.hpp"
# include	"Bomb.class.hpp"
# include	"Wall.class.hpp"
# include   "Enemy.class.hpp"
# include   "Player.class.hpp"
# include	"Powerup.class.hpp"
# include	"../DisplayEntitiesController.hpp"
# include 	"Sound.hpp"
# include	"Music.hpp"

class Map {

public:
	Map( unsigned int width, unsigned int height, animation::DisplayEntitiesController	*displayEntitiesController, int level = 1 );
	Map() = default;
	Map( Map &object );
	~Map( void );
	Map &operator=( Map const &object );

	void	spawnBomb( Location l, float countdown = 2, unsigned int range = 1 );
	void	spawnWall( Location l, E_WallType type = E_INVULNERABLE );
	void	spawnEnemy( Location l );
	void	spawnPowerup( Location l, E_Powerup type = E_FireUp );

	void 	loadMusicForLevel();

	void	handleBombs( void );
	void	handleEnemies( void );
	void	handleWalls( void );
	void	handlePlayer( void );
	void	handlePowerups( void );

	bool	glmLocationEq( glm::vec3 a, glm::vec3 b, float offset = 1 );

	bool	checkCollisionWalls( Location const & l );
	bool	checkCollisionPlayer( Location const & l );
	bool	checkCollisionBombs( Location const & l );
	bool	checkCollisionEnemy( Location const & l );
	bool	checkCollisionEnemyAnim( glm::vec3 const & anim_l, float offset = 1 );
	bool	checkCollisionPlayerAnim( glm::vec3 const & anim_l, float offset = 1 );
	bool	checkCollisionPowerup( Location const & l );

	std::vector<Wall *>		getWalls( void );
	std::vector<Bomb *>		getBombs( void );
	std::vector<Enemy *>	getEnemies( void );
	std::vector<Powerup *>	getPowerups( void );

	unsigned int			m_worldWidth;
	unsigned int			m_worldHeight;
	int 					m_level;

	void		increaseEnemyDifficulty( void );
	void		addHiddenDoor( void );
	void		generateWorld( void );

	bool		getNextLevelFlag( void );

	void		clear();

	void 		checkVolume();
	void 		playMusic();

private:
	void 		destroyWall(int index);
	void 		destroyBomb(int index);
	void 		destroyEnemy(int index);
	void 		destroyPowerup(int index);

	void		tickBombs( void );
	void		explodeExpiredBombs( void );
	void		spawnBombTrail(unsigned int range, Location sp, int axis, int dir);

	Location	getRandomDst( unsigned int range = 0 );

	void		tryDamagePlayer( void );
	void		tryMoveEnemies( void );
	void		tryDestroyEnemy( void );
	void		tryDestroyWall( void );

	std::vector<Powerup *>	m_powerups;
	std::vector<Wall *>		m_walls;
	std::vector<Bomb *>		m_bombs;
	std::vector<Enemy *>	m_enemies;
	Player					*m_player;

	bool					m_nextLevel;
	Music					*m_levelMusic;
	Sound  					m_bomb_explode_sound;
    Sound					m_fire_range_increase_sound;
    Sound					m_health_increase_sound;
    Sound					m_next_level_sound;
    Sound					m_cannot_enter_sound;
    Sound					m_power_up_spawn_sound;
    Sound					m_door_spawn_sound;

	bool					m_playing;

	animation::DisplayEntitiesController	*displayEntitiesController;

	template <class T>
	void	destroyElem(std::vector<T *> &array, int index)  {
		array.erase(array.begin() + index);
	}

};

#endif
