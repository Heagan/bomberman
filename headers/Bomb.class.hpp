#ifndef BOMB_H
# define BOMB_H

# include "bomber.h"
# include "Location.class.hpp"

class Bomb {

public:
	Bomb( Location location, float countdownInSeconds = 2, Uint32 range = 1, float explosionDurationInSeconds = 0 );
	Bomb( Bomb &object );
	~Bomb( void );
	Bomb &operator=( Bomb const &object );

	void		tick( void );
	bool 		checkActive(void);

	Location		&getLocation( void );
	unsigned int 	getRange( void );
	bool			functionToCheckIfTheBombIsStillExploding( void );

	int 			bombIndex;

private:
	void		explode();
	
	Location	m_location;
	Uint32		m_spawnTime;
	Uint32		m_range;
	bool		m_active;
	float		m_countdownInMilliseconds;
	float		m_explosionDurationInMilliseconds;

 };

#endif
