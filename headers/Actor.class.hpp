#ifndef Actor_CLASS_H
# define Actor_CLASS_H

# include	"bomber.h"
# include	"Location.class.hpp"

class Actor {

public:
	Actor() = default;
	Actor(Actor &) = default;
	Actor& operator= (Actor &) = default;
	~Actor() = default;
	std::string 	getName() { return this->m_name; };

	std::string m_name;
	float		m_health;
	Location	m_location;
	Location	m_dst;
	E_ActorType	m_type;

protected:

};

#endif
