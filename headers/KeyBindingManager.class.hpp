#ifndef KEY_BINDING_MANAGER_H
# define KEY_BINDING_MANAGER_H

# include "bomber.h"

class KeyBindingManager {

public:
	KeyBindingManager( void );
	KeyBindingManager( KeyBindingManager &object );
	~KeyBindingManager( void );
	KeyBindingManager &operator=( KeyBindingManager const &object );

	int		getValueFromKey(E_Input key );
	void	setKeyToValue(E_Input key, int keyCode);

private:
	std::map<E_Input, int>	m_keys;

};

#endif
