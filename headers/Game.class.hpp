#ifndef GAME_H
# define GAME_H

# include "bomber.h"
# include "KeyBindingManager.class.hpp"
# include "Map.class.hpp"
# include "Player.class.hpp"
# include "Bomb.class.hpp"
# include "Music.hpp"
# include "Sound.hpp"

# include "../DisplayEntitiesController.hpp"
#include "../InGamePlayTextDisplay/InGamePlayTextDisplay.hpp"
#include "../GUI/GUI.hpp"

class Game {

public:
	Game( void );
	Game( Game &object );
	~Game( void );
	Game &operator=( Game const &object ) ;

	void	update( void );
	void	render( void );

	void	startGame( void );
	void	keyManger();
	void	saveData();
	void	updateAnimation(Location l, int index);

public:
	void		drawGuiText(void);

	void		loadMap( void );
	void		loadSaveData( void );
	
	void		gameLoop(void);

	void 		initCamera( void );

	void 		checkPlayer( void );
	void		handleMapProgress( void );
	void		updateTimeRemaining( void );
	void		tryMovePlayer(int x, int y);

	void 		createWorldFloor( void );
	void 		displayWinGame( void );
	void 		displayGameOver( void );

	unsigned long	getTimeRemaining( void );

	int			m_level;

	Map			*m_map;
	Player		*m_player;

	KeyBindingManager	m_controls;
	const unsigned int	m_gameCountdownInMilliseconds = 200 * 1000;
	unsigned long		m_levelStartTime;
	unsigned long 		m_unpausedTime;
	unsigned long		m_pausedTime;
	unsigned long		m_timeRemaining;
	unsigned long		m_totalPauseDuration;

	GUI					*m_gui;

	Window				*m_gameWindow;
	ModelDispenser		*m_modelDispenser;
	Renderer			*m_renderer;
	Camera				*m_camera;
	glm::vec3			m_cameraTrans;
	InGamePlayTextDisplay					*m_inGamePlayTextDisplay;
	animation::DisplayEntitiesController	*displayEntitiesController;
	bool 				m_start;
	Music				m_theme;
	Sound				m_bounce;
	Sound				m_victory_sound;
	Sound				m_lose_sound;
	bool 				m_running;
	bool 				loadedData;


	SDL_Event event;

};

#endif
