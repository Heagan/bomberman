#include "DisplayEntitiesController.hpp"

namespace animation {

    modelWrapper::modelWrapper() {

    };

    modelWrapper::modelWrapper(ModelDispenser *modelDispenser, Shader *shaderProgram, std::string fname,
                               std::string path) : m_shaderProgram(shaderProgram) {
        this->m_character = modelDispenser->getModel(fname, path);
        this->m_file_name = fname;
    }

    modelWrapper::~modelWrapper() {
        delete this->m_character;
    }

    void modelWrapper::update() {
        m_character->draw(*m_shaderProgram);
    }

    double nothing( double rot)
    {
        return rot;
    }

	Animation modelWrapper::makeAnim(std::string animeName, double dir, double rot, double axis) {
		Animation anim;

		nothing(rot);
		Transformation keyFrame1Transformation;
		KeyFrame keyFrame1(0.0f, keyFrame1Transformation);
		keyFrame1Transformation.m_rotation = glm::vec3(rot * (axis == 0), rot * (axis == 1), rot * (axis == 2));

		Transformation keyFrame2Transformation;
		keyFrame2Transformation.m_translation = glm::vec3(dir * (axis == 0), dir * (axis == 1), dir * (axis == 2));
		KeyFrame keyFrame2(0.10f, keyFrame2Transformation);

		dir *= 2;
		Transformation keyFrame3Transformation;
		keyFrame3Transformation.m_translation = glm::vec3(dir * (axis == 0), dir * (axis == 1), dir * (axis == 2));
		KeyFrame keyFrame3(0.20f, keyFrame3Transformation);

		anim.addKeyFrame(keyFrame1);
		anim.addKeyFrame(keyFrame2);
		anim.addKeyFrame(keyFrame3);

		std::string animationName = animeName;

		return anim;
	}

	Animation		modelWrapper::makeAnimRotate(std::string animeName, double rot, double axis) {
		Animation anim;

		Transformation keyFrame1Transformation;
		keyFrame1Transformation.m_rotation = {rot * (axis == 0), rot * (axis == 1), rot * (axis == 2)};
		KeyFrame keyFrame1(0.0f, keyFrame1Transformation);

		rot *= 2;

		Transformation keyFrame2Transformation;
		keyFrame2Transformation.m_rotation = {rot * (axis == 0), rot * (axis == 1), rot * (axis == 2)};
		KeyFrame keyFrame2(0.1f, keyFrame2Transformation);

		anim.addKeyFrame(keyFrame1);
		anim.addKeyFrame(keyFrame2);

		std::string animationName = animeName;

		return anim;

    }

	Animation		modelWrapper::makeAnimBouncy(std::string animeName, double dir, double rot, double axis) {
		Animation anim;

        nothing(rot);

		Transformation keyFrame1Transformation;

		KeyFrame keyFrame1(0.0f, keyFrame1Transformation);

		Transformation keyFrame2Transformation;
		keyFrame2Transformation.m_translation = glm::vec3(0.1 * (axis == 0) * dir, -0.2, 0.1 * (axis == 2) * dir);
		keyFrame2Transformation.m_scaling = glm::vec3(1.2, 0.6, 1.2);
		keyFrame2Transformation.m_rotation = keyFrame1Transformation.m_rotation;
		KeyFrame keyFrame2(0.10f, keyFrame2Transformation);

		Transformation keyFrame3Transformation;
		keyFrame3Transformation.m_translation = glm::vec3(0.15 * (axis == 0) * dir, 0.7, 0.15 * (axis == 2) * dir);
		keyFrame3Transformation.m_scaling = glm::vec3(0.8, 1.4, 0.8);
		keyFrame3Transformation.m_rotation = keyFrame1Transformation.m_rotation;
		KeyFrame keyFrame3(0.20f, keyFrame3Transformation);

		Transformation keyFrame4Transformation;
		keyFrame4Transformation.m_translation = glm::vec3(0.2 * (axis == 0) * dir, 1.5, 0.2 * (axis == 2) * dir);
		keyFrame4Transformation.m_rotation = keyFrame1Transformation.m_rotation;
		KeyFrame keyFrame4(0.25f, keyFrame4Transformation);

		Transformation keyFrame5Transformation;
		keyFrame5Transformation.m_translation = glm::vec3(0.4 * (axis == 0) * dir, -0.2, 0.4 * (axis == 2) * dir);
		keyFrame5Transformation.m_scaling = glm::vec3(1.2, 0.6, 1.2);
		keyFrame5Transformation.m_rotation = keyFrame1Transformation.m_rotation;
		KeyFrame keyFrame5(0.35f, keyFrame5Transformation);

		Transformation keyFrame6Transformation;
		keyFrame6Transformation.m_translation = glm::vec3(0.4 * (axis == 0) * dir, 0, 0.4 * (axis == 2) * dir);
		keyFrame6Transformation.m_rotation = keyFrame1Transformation.m_rotation;
		KeyFrame keyFrame6(0.37f, keyFrame6Transformation);


		anim.addKeyFrame(keyFrame1);
		anim.addKeyFrame(keyFrame2);
		anim.addKeyFrame(keyFrame3);
		anim.addKeyFrame(keyFrame4);
		anim.addKeyFrame(keyFrame5);
		anim.addKeyFrame(keyFrame6);

		std::string animationName = animeName;

		return anim;
    }

    Transformation	modelWrapper::playAnim(std::string name) {
        if (this->testNotificationClass.m_isActive == false) {
            this->m_character->startAnimation(name);
            this->testNotificationClass.m_isActive = true;
        }
        return this->m_character->getModelTransformation();
    }

	glm::vec3		modelWrapper::getLocation() {
    	return this->m_character->getModelTransformation().m_translation;
    }

	Movable::Movable() {

	};

	Movable::~Movable() {

	}

	Movable::Movable(ModelDispenser *modelDispenser, Shader *shaderProgram, std :: string fname, std :: string  path, int x, int y, int z) : modelWrapper(modelDispenser, shaderProgram, fname, path) {
		this->m_character = modelDispenser->getModel(fname, path);
		this->m_file_name = fname;

		this->m_character->translate(glm::vec3(x, y, z));

		moveLeftAnim = this->makeAnimBouncy(std::string("LeftAnim"), -5, 90, 0);
		this->m_character->addAnimation(moveLeftAnim, "LeftAnime");

		moveRightAnim = this->makeAnimBouncy("RightAnim", 5, -90, 0);
		this->m_character->addAnimation(moveRightAnim, "RightAnime");

		moveUpAnim = this->makeAnimBouncy("UpAnime", 5, -90, 2);
		this->m_character->addAnimation(moveUpAnim, "UpAnime");

		moveDownAnim = this->makeAnimBouncy("DownAnime", -5, 90, 2);
		this->m_character->addAnimation(moveDownAnim, "DownAnime");

		this->m_character->registerObserver(&this->testNotificationClass);
	}

	void Movable :: moveLeft() {
		playAnim("LeftAnime");
        m_character->rotate({0, 270, 0});

    }

	void Movable :: moveRight() {
		playAnim("RightAnime");
        m_character->rotate({0, 90, 0});

    }

	void Movable :: moveUp() {
		playAnim("UpAnime");
        m_character->rotate({0, 0, 0});

    }

	void Movable :: moveDown() {
		playAnim("DownAnime");
        m_character->rotate({0, 180, 0});

    }

    nonMovable::nonMovable() {

    }

    nonMovable::~nonMovable() {

    }

    nonMovable::nonMovable(ModelDispenser *modelDispenser, Shader *shaderProgram, std::string fname, std::string path, int x, int y, int z) : modelWrapper(modelDispenser, shaderProgram, fname, path) {

		m_shaderProgram = shaderProgram;
		m_modelDispenser = modelDispenser;
		m_file_name = fname;

        this->m_character = modelDispenser->getModel(fname, path);
        this->m_file_name = fname;

        this->m_character->translate(glm::vec3(x, y, z));

        AnimeList["DIE"] = this->makeAnim(std::string("DIE"), -2, 0, 1);
        this->m_character->addAnimation(AnimeList["DIE"], std::string("DIE"));

		AnimeList["grow"] = this->makeGrowAnim();
		this->m_character->addAnimation(AnimeList["grow"], std::string("grow"));

		AnimeList["explode"] = makeExplodeAnim();
		this->m_character->addAnimation(AnimeList["explode"], std::string("explode"));

		AnimeList["rotate"] = makeAnimRotate("Rotate", 5, 1);
		this->m_character->addAnimation(AnimeList["rotate"], std::string("rotate"));

		this->m_character->registerObserver(&this->testNotificationClass);
    }

	Animation	nonMovable::makeGrowAnim() {
		KeyFrame growKeyFrame(0.0f, Transformation());
		Transformation explodeTrans;
		explodeTrans.m_scaling = {0.5, 0.5, 0.5};

		KeyFrame growKeyFrame2(0.25f, explodeTrans);
		Transformation explodeTrans2;
		explodeTrans2.m_scaling = {1, 1, 1};

		KeyFrame growKeyFrame3(0.5f, explodeTrans2);
		Transformation explodeTrans3;
		explodeTrans3.m_scaling = {5, 5, 5};

		Animation growAnimation;
		growAnimation.addKeyFrame(growKeyFrame);
		growAnimation.addKeyFrame(growKeyFrame2);
		growAnimation.addKeyFrame(growKeyFrame3);

		return growAnimation;
	}

	Animation 	nonMovable::makeExplodeAnim() {
		KeyFrame explodeKeyFrame(0.0f, Transformation());
		Transformation explodeTrans;
		explodeTrans.m_scaling = {1, 1, 1};
		KeyFrame explodeKeyFrame2(0.15f, explodeTrans);
		Transformation explodeTrans2;
		explodeTrans2.m_scaling = {0, 0, 0};
		KeyFrame explodeKeyFrame3(0.3f, explodeTrans2);
		Transformation explodeTrans3;

		Animation explodeAnimation;
		explodeAnimation.addKeyFrame(explodeKeyFrame);
		explodeAnimation.addKeyFrame(explodeKeyFrame2);
		explodeAnimation.addKeyFrame(explodeKeyFrame3);

		return explodeAnimation;
	}

    void nonMovable:: playAnimation(std :: string name) {
        this->m_isAnimationActive = true;
		playAnim(name);
    }

	void nonMovable::notify(void *arg) {
		assert(arg == nullptr);
		this->m_isAnimationActive = false;
	}

	bool nonMovable::isAnimationActive() {
		return this->m_isAnimationActive;
	}

	int		DisplayEntitiesController::createMovable(int x, int y, int z, std::string model) {
		static int index = 0;

		movables[index] = new animation::Movable(modelDispenser, shaderProgram, model,"assets", x, y, z);
		index++;
		return index - 1;
	}

	int		DisplayEntitiesController::createNonMovable(int x, int y, int z, std::string model) {
		static int index = 0;

		nonMovables[index] = new animation::nonMovable(modelDispenser, shaderProgram, model,"assets", x, y, z);
		index++;
		return index - 1;
	}

	void	DisplayEntitiesController::destroyMovable(int index) {
		delete movables[index];
		movables.erase(index);
	}

	void 	DisplayEntitiesController::destroyNonMovable(int index) {
		delete nonMovables[index];
		nonMovables.erase(index);
	}
}

