//
// Created by lx on 9/24/18.
//

#ifndef OPENGLLEARNING_SOUND_HPP
#define OPENGLLEARNING_SOUND_HPP

#include <SFML/Audio.hpp>
#include "../../headers/bomber.h"

class Sound {
public:
    Sound();
    Sound(std::string fileName);
    Sound (Sound &) = default;
    Sound& operator = (Sound &) = default;
    ~Sound();
    void playSound();
    void changeVolume(int volume);

private:
    sf::SoundBuffer buffer;
    sf::Sound sound;
    std::string file;
};


#endif //OPENGLLEARNING_SOUND_HPP
