//
// Created by lx on 9/24/18.
//

#ifndef OPENGLLEARNING_MUSIC_HPP
#define OPENGLLEARNING_MUSIC_HPP

#include <SFML/Audio.hpp>

class Music {
public:
    Music(std::string fileName);
    Music() = default;
    Music (Music &) = default;
    Music& operator = (Music &) = default;
    ~Music();
    void playMusic();
    void stopMusic();
    void changeVolume(int volume);

private:
    sf::Music music;
    std::string file;
};


#endif //OPENGLLEARNING_MUSIC_HPP
