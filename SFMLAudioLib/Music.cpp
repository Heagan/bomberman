//
// Created by lx on 9/24/18.
//

#include "Includes/Music.hpp"

Music::Music(std::string fileName) {
    this->file = fileName;
    this->music.openFromFile(fileName);
    this->music.setLoop(true);
}

Music::~Music() {}

void Music::playMusic() {
    this->music.play();
}

void Music::stopMusic() {
    this->music.stop();
}

void Music::changeVolume(int volume) {
    this->music.setVolume(volume);
}