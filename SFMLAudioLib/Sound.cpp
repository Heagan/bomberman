//
// Created by lx on 9/24/18.
//

#include "Includes/Sound.hpp"

Sound::Sound(std::string fileName) {
    this->file = fileName;
    this->buffer.loadFromFile(this->file);
    this->sound.setBuffer(this->buffer);
}

Sound::Sound() {}

Sound::~Sound() {}

void Sound::playSound() {
    this->sound.play();
}

void Sound::changeVolume(int volume) {
    this->sound.setVolume(volume);
}