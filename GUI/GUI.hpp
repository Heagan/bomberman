//
// Created by lx on 9/12/18.
//

#ifndef LEARNINGOPENGL_RENDERGUI_HPP
#define LEARNINGOPENGL_RENDERGUI_HPP

#include "imgui/imgui.h"
#include "imgui/imgui_impl_sdl.h"
#include "imgui/imgui_impl_opengl3.h"

#include <SDL_video.h>
#include "bomber.h"

enum E_CONTROL_CHANGE {E_DEFAULT, E_UP, E_DOWN, E_LEFT, E_RIGHT, E_BOMB};

class GUI {
public:
	GUI() = default;
    GUI(Window &window, SDL_GLContext &gl_context);
    GUI(GUI &) = default;
    GUI & operator = (GUI &) = default;
    ~GUI();
    void gameMenu(int windowHeight, int windowWidth);
    E_CONTROL_CHANGE getChangedKey();
	void debugMenu();
    void createFrame();
    void render();
    void resetGUI();
    void clean();
    void pause();
    void unpause();
    bool isPaused();
    bool hasStarted();
    bool hasEnded();
    bool shouldLoad();
    bool shouldSave();
    void resetChangedKey();
    bool shouldQuitToMainMenu();
	bool getDebugStatus();
	void setDebugStatus(bool status);
	bool wasStartNewGamePressed();

private:
	Window *window;
    SDL_GLContext context;
    E_CONTROL_CHANGE changedKey;
    ImFont *gameFont;
    ImFont *defaultFont;
    bool mainMenuActive;
    bool optionsMenuActive;
    bool pauseMenuActive;
    bool gameStarted;
    bool gamePaused;
	bool gameEnded;
	bool loadGame;
	bool saveGame;
	bool wasNewGamePressed;
    bool quitToMainMenu;
	bool debug;
};


#endif //LEARNINGOPENGL_RENDERGUI_HPP
