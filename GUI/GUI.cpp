//
// Created by lx on 9/12/18.
//

#include <Window.hpp>
#include <string>
#include "GUI.hpp"

#if __APPLE__
#define GLSL_VERSION "#version 150"
#else
#define GLSL_VERSION "#version 130"
#endif

extern int G_VOLUME;

		GUI::GUI(Window &window, SDL_GLContext &gl_context) {
			this->saveGame	= false;
			this->loadGame	= false;
			this->window	= &window;
			this->context	= gl_context;
            this->changedKey	= E_DEFAULT;
			this->mainMenuActive	= true;
			this->optionsMenuActive = false;
			this->pauseMenuActive	= false;
			this->gameStarted	= false;
			this->gameEnded		= false;
			this->gamePaused	= false;
            this->quitToMainMenu = false;
			this->debug         = false;

			IMGUI_CHECKVERSION();
			ImGui::CreateContext();
			ImGuiIO& io = ImGui::GetIO(); (void)io;
			ImGui_ImplSDL2_InitForOpenGL(window.getWindowPtr(), gl_context);
			ImGui_ImplOpenGL3_Init(GLSL_VERSION);

			ImGui::StyleColorsDark();

            this->gameFont = ImGui::GetIO().Fonts->AddFontFromFileTTF("assets/Heavy/heavy_data.ttf", 26.0f);
            this->defaultFont = ImGui::GetIO().Fonts->AddFontDefault();
		}

		GUI::~GUI() {
			ImGui_ImplOpenGL3_Shutdown();
			ImGui_ImplSDL2_Shutdown();
			ImGui::DestroyContext();
		}

		void GUI::debugMenu() {
            ImGui::SetNextWindowPos(ImVec2(0, 0));
            ImGui::SetNextWindowSize(ImVec2(500, 100));
            if (this->debug) {
                ImGui::PushFont(defaultFont);
                ImGui::PopStyleColor();
                ImGui::PushStyleColor(ImGuiCol_WindowBg, {0, 0, 0, 0});
                ImGui::Begin("Debug Menu", nullptr,
                             ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar);
                ImGui::SetNextWindowPos(ImVec2(0, 0));
                ImGui::Text("Application Average: %.3f ms/frame (%.1f FPS)", 1000.f / ImGui::GetIO().Framerate,
                            ImGui::GetIO().Framerate);
                ImGui::End();
                ImGui::PopStyleColor();
                ImGui::PushStyleColor(ImGuiCol_WindowBg, {0, 0, 0, 0.75f});
                ImGui::PopFont();
            }
		}

		void GUI::gameMenu(int width, int height) {

			int windowWidth = width / 4;
			int windowHeight = height / 4;

			ImGui::SetNextWindowPos(ImVec2((width / 2) - (windowWidth / 2), (height / 2) - (windowHeight / 2))); // width, height
			ImGui::SetNextWindowSize(ImVec2(windowWidth * 1.2, height / 2)); // width, height.

			// MainMenu
            if (this->mainMenuActive) {
                ImGui::Begin("Main Menu", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar);
                ImGui::PushFont(gameFont);

                if (ImGui::Button("New Game", ImVec2(windowWidth - 15, 50))) {
                    this->loadGame		= false;
                    this->gameStarted	= true;
                    this->gamePaused	= false;
                    this->mainMenuActive 	= false;
                    this->optionsMenuActive = false;
                    this->pauseMenuActive 	= false;
                    this->wasNewGamePressed = true;
                }
                if (ImGui::Button("Load Game", ImVec2(windowWidth - 15, 50))) {
                    this->loadGame		= true;
                    this->gameStarted	= true;
                    this->gamePaused	= false;
                    this->mainMenuActive	= false;
                    this->optionsMenuActive = false;
                    this->pauseMenuActive	= false;
                }
                if (ImGui::Button("Options", ImVec2(windowWidth - 15, 50))) {
                    this->optionsMenuActive = true;
                    this->mainMenuActive = false;
                }
                if (ImGui::Button("Quit", ImVec2(windowWidth - 15, 50))) {
                    this->gameEnded = true;
                }
                ImGui::PopFont();
                ImGui::End();
            }

            // OptionsMenu
            if (this->optionsMenuActive) {
                ImGui::SetNextWindowPos(ImVec2((width / 2) - (windowWidth / 2), ((height - (height / 3)) / 2) - (windowHeight / 2)));
                ImGui::Begin("Options Menu", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_HorizontalScrollbar);
                ImGui::PushFont(gameFont);
                ImGui::Text("Options");

                if (ImGui::Button("Back")) {
                    if (!this->hasStarted()) {
                        this->mainMenuActive = true;
                    }
                    if (this->isPaused()) {
                        this->pauseMenuActive = true;
                    }
                    this->optionsMenuActive = false;
                }

                ImGui::Text("Volume");
                ImGui::SliderInt("##volume", &G_VOLUME, 0, 100);
                std::cout << "Slider: " << G_VOLUME << std::endl;

                ImGui::Text("Key Bindings");
                ImGui::BeginGroup();
                if (ImGui::Button("Up")) {
                    this->changedKey = E_UP;
                }
                ImGui::SameLine();
                if (ImGui::Button("Down")) {
                    this->changedKey = E_DOWN;
                }
                ImGui::SameLine();
                if (ImGui::Button("Left")) {
                    this->changedKey = E_LEFT;
                }
                ImGui::SameLine();
                if (ImGui::Button("Right")) {
                    this->changedKey = E_RIGHT;
                }
                ImGui::SameLine();
                if (ImGui::Button("Drop Bomb")) {
                    this->changedKey = E_BOMB;
                }
                if(this->changedKey != E_DEFAULT) {
                    ImGui::Text("Press the key you want to rebind to!");
                }
                ImGui::EndGroup();

                static bool toggleFullscreen = false;
                static bool shouldToggleFullscreen = false;

                ImGui::Text("Fullscreen");
                ImGui::Checkbox("##fullscreen", &toggleFullscreen);

                ImGui::Text("Resolution");
                if (toggleFullscreen != shouldToggleFullscreen) {
                    this->window->toggleFullScreen();
                    shouldToggleFullscreen = toggleFullscreen;
                }

                for (unsigned int i = 0; i < this->window->getValidResolutionList().size(); ++i) {
                    std::string s;

                    s = std::to_string(this->window->getValidResolutionList()[i][0]);
                    s += "x";
                    s += std::to_string(this->window->getValidResolutionList()[i][1]);
                    if (ImGui::Button(s.c_str())) {
                        this->window->setResolution(i);
                    }
                }

                ImGui::Text("Debug");
                ImGui::Checkbox("##debug", &this->debug);

                ImGui::PopFont();
                ImGui::End();
            }

            // Pause Menu
            if (this->pauseMenuActive) {
                int pauseMenuHeight = height / 4;
                ImGui::SetNextWindowPos(ImVec2((width / 2) - (windowWidth / 2), ((height - (height / 3)) / 2) - (windowHeight / 2)));
                ImGui::SetNextWindowSize(ImVec2(windowWidth, pauseMenuHeight));
                ImGui::Begin("Game Paused", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar);
                ImGui::PushFont(gameFont);
                ImGui::Text("Paused");
                if (ImGui::Button("Save")) {
                    this->saveGame = true;
                }
                if (ImGui::Button("Options")) {
                    this->optionsMenuActive = true;
                    this->pauseMenuActive = false;
                }
                if (ImGui::Button("Quit")) {
                    if (this->mainMenuActive) {
                        this->gameEnded = true;
                    }
                    else if (this->pauseMenuActive)
                    {
                        this->quitToMainMenu = true;
                    }
                }
                ImGui::PopFont();
                ImGui::End();
            }

            ImGui::SetNextWindowPos(ImVec2((width / 2) - (windowWidth / 2), (height / 2) - (windowHeight / 2))); // width, height
}

void GUI::createFrame() {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(this->window->getWindowPtr());
    ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 100.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
    ImGui::PushStyleColor(ImGuiCol_WindowBg, {0, 0, 0, 0.75f});
    ImGui::NewFrame();
};

void GUI::render() {
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

void GUI::clean() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();
}

void GUI::resetGUI() {
	this->mainMenuActive = true;
	this->optionsMenuActive = false;
	this->pauseMenuActive = false;
	this->gameStarted = false;
	this->gameEnded = false;
	this->gamePaused = false;
	this->loadGame = false;
	this->changedKey = E_DEFAULT;
}

void GUI::pause() {
	if (!this->gameStarted) {
		return ;
	}
    this->pauseMenuActive = true;
    this->gamePaused = true;
}

void GUI::unpause() {
    this->pauseMenuActive = false;
    this->optionsMenuActive = false;
    this->gamePaused = false;
}

bool GUI::isPaused() {
    return (this->gamePaused);
}

bool GUI::hasStarted() {
    return (this->gameStarted);
}

bool GUI::hasEnded() {
	return (this->gameEnded);
}

E_CONTROL_CHANGE GUI::getChangedKey() {
	return (this->changedKey);
}

void GUI::resetChangedKey() {
	this->changedKey = E_DEFAULT;
}

bool GUI::shouldLoad() {
	if (loadGame) {
		loadGame = false;
		return true;
	}
	return this->loadGame;
}

bool GUI::shouldSave() {
	if (saveGame) {
		saveGame = false;
		return true;
	}
	return false;
}

bool GUI::shouldQuitToMainMenu() {
	return (this->quitToMainMenu);
}

bool GUI::getDebugStatus() {
    return (this->debug);
}

void GUI::setDebugStatus(bool status) {
    this->debug = status;
}

bool GUI::wasStartNewGamePressed() {
	if (this->wasNewGamePressed) {
		this->wasNewGamePressed = false;
		return true;
	}
	return false;
}
