#include "Map.class.hpp"
#include "Sound.hpp"


Map::Map( unsigned int width, unsigned int height, animation::DisplayEntitiesController	*displayEntitiesController, int level  ) :m_level(level), m_bomb_explode_sound("assets/sound/explodiGood.wav"), m_fire_range_increase_sound("assets/sound/rangeUp.wav"), m_health_increase_sound("assets/sound/healthUp.wav"),  m_next_level_sound("assets/sound/nextLevel.wav"), m_cannot_enter_sound("assets/sound/can_not_go_through.wav"), m_power_up_spawn_sound("assets/sound/powerUpSpawn.wav"), m_door_spawn_sound("assets/sound/nextLevel.wav") {
	this->displayEntitiesController = displayEntitiesController;
	this->m_worldWidth = width;
	this->m_worldHeight = height;
	this->m_player = Player::getInstance();
	this->m_nextLevel = false;
	this->m_playing = false;
	this->loadMusicForLevel();
}

Map::Map( Map &object ) {
	*this = object;
}

Map::~Map( void ) {
	while (!m_walls.empty())
		destroyWall(0);
	while (!m_bombs.empty())
		destroyBomb(0);
	while (!m_powerups.empty())
		destroyPowerup(0);
	while (!m_enemies.empty())
		destroyEnemy(0);

	m_levelMusic->stopMusic();
}

Map	&Map::operator=( Map const &object ) {
	this->m_worldWidth = object.m_worldWidth;
	this->m_worldHeight = object.m_worldHeight;

	this->m_walls = object.m_walls;
	this->m_bombs = object.m_bombs;
	this->m_enemies = object.m_enemies;
	this->m_player = object.m_player;
	this->m_powerups = object.m_powerups;
	this->m_nextLevel = object.m_nextLevel;
	return *this;
}

void 	Map::checkVolume() {
	if (G_VOLUME > 0)
		m_levelMusic->changeVolume(G_VOLUME - 40);
	else
		m_levelMusic->changeVolume(0);
}

void 	Map::loadMusicForLevel() {
	if (m_level == 1) {
		m_levelMusic = new Music("assets/music/battle.wav");
	}
	if (m_level == 2) {
		m_levelMusic = new Music("assets/music/hero.wav");
	}
	if (m_level == 3) {
		m_levelMusic = new Music("assets/music/redial.wav");
	}
}

void 	Map::playMusic() {
	if (m_playing) {
		return ;
	}
	m_playing = true;
	m_levelMusic->playMusic();
}

void	Map::clear() {
	while (!m_walls.empty())
		destroyWall(0);
	while (!m_bombs.empty())
		destroyBomb(0);
	while (!m_powerups.empty())
		destroyPowerup(0);
	while (!m_enemies.empty())
		destroyEnemy(0);
}

void	Map::spawnBomb( Location l, float countdown, unsigned int range ) {
	for (auto &wall : m_walls) {
		 if (wall->location == l) {
		 	if (wall->type == E_INVULNERABLE) {
		 		return ;
		 	}
		 }
	}

	if (range > 0) {
		if (checkCollisionBombs(l)) {
			return;
		}
	}
	this->m_bombs.push_back(new Bomb(l, countdown, range));

	if (range > 0) {
		this->m_bombs.back()->bombIndex = displayEntitiesController->createNonMovable(l.x * 2, 0, l.y * 2, "non_movables/bomb.obj");
		displayEntitiesController->getnonMovable(this->m_bombs.back()->bombIndex)->playAnimation("grow");
	}
	else {
		this->m_bombs.back()->bombIndex = displayEntitiesController->createNonMovable(l.x * 2, 0, l.y * 2, "non_movables/explosion.obj");
		displayEntitiesController->getnonMovable(this->m_bombs.back()->bombIndex)->playAnimation("explode");
	}
}

void	Map::spawnWall( Location l, E_WallType type ) {
	this->m_walls.push_back(new Wall(l, type));

	std::string		destruc_wall	= "non_movables/Crate.obj";
	std::string		indestruc_wall	= "non_movables/box_simple.obj";

	if (m_level == 2) {
		destruc_wall	= "non_movables/Crate.obj";
		indestruc_wall	= "non_movables/nether.obj";
	}
	if (m_level == 3) {
		destruc_wall	= "non_movables/cubo.obj";
		indestruc_wall	= "non_movables/end.obj";
	}

	if (type == E_INVULNERABLE)
		this->m_walls.back()->m_modelIndex = displayEntitiesController->createNonMovable(l.x * 2, 0, l.y * 2, indestruc_wall);
	else
		this->m_walls.back()->m_modelIndex = displayEntitiesController->createNonMovable(l.x * 2, 20, l.y * 2, destruc_wall);
}

void	Map::spawnEnemy( Location l ) {
	std::string models[] = {
			"movables/dark_slime.obj",
			"movables/water_slime.obj",
			"movables/fire_slime.obj",
			"movables/fire_baby_slime.obj",
			"movables/water_baby_slime.obj",
			"movables/orange_baby_slime.obj",
			"movables/yellow_slime.obj",
			"movables/purple_slime.obj",
	};


	this->m_enemies.push_back(new Enemy(l));
	this->m_enemies.back()->m_enemyModelIndex = displayEntitiesController->createMovable(l.x * 2, 0, l.y * 2, models[std::rand() % 8]);
}

void	Map::spawnPowerup( Location l, E_Powerup type ) {
	this->m_powerups.push_back(new Powerup(l, type));
	switch (type) {
		case (E_FireUp) : {

			if (G_VOLUME > 0)
				this->m_power_up_spawn_sound.changeVolume(G_VOLUME + 10);
			else
				this->m_power_up_spawn_sound.changeVolume(G_VOLUME);
			this->m_power_up_spawn_sound.playSound();
			this->m_powerups.back()->powerUpIndex = displayEntitiesController->createNonMovable(l.x * 2, 0, l.y * 2, "non_movables/fire_item.obj");
			break ;
		}
		case (E_OneUp) : {
			if (G_VOLUME > 0)
				this->m_power_up_spawn_sound.changeVolume(G_VOLUME + 10);
			else
				this->m_power_up_spawn_sound.changeVolume(G_VOLUME);
			this->m_power_up_spawn_sound.playSound();
			this->m_powerups.back()->powerUpIndex = displayEntitiesController->createNonMovable(l.x * 2, 0, l.y * 2, "non_movables/heart_item.obj");
			break ;
		}
		case (E_DoorClosed) : {
			this->m_powerups.back()->powerUpIndex = displayEntitiesController->createNonMovable(l.x * 2, 0, l.y * 2, "non_movables/red_trap_door.obj");
			if (G_VOLUME > 0)
				this->m_door_spawn_sound.changeVolume(G_VOLUME + 10);
			else
				this->m_door_spawn_sound.changeVolume(G_VOLUME);
			this->m_door_spawn_sound.playSound();
			displayEntitiesController->getnonMovable(this->m_powerups.back()->powerUpIndex)->playAnim("grow");
			break ;
		}
		case (E_DoorOpen) : {
			this->m_powerups.back()->powerUpIndex = displayEntitiesController->createNonMovable(l.x * 2, 0, l.y * 2, "non_movables/yellow_trap_door.obj");
			displayEntitiesController->getnonMovable(this->m_powerups.back()->powerUpIndex)->playAnim("grow");
			break ;
		}
		case (E_None) : {
			break ;
		}
	}
}

bool	Map::checkCollisionEnemy( Location const & l ) {
	for (size_t i = 0; i < this->m_enemies.size(); i++ ) {
		if (this->m_enemies.at(i)->m_location == l) {
			return true;
		}
	}
	return false;
}

bool	Map::glmLocationEq( glm::vec3 a, glm::vec3 b, float offset ) {
	if (abs(a.x - b.x) < offset ) {
		if (abs(a.y - b.y) < offset ) {
			if (abs(a.z - b.z) < offset ) {
				return true;
			}
		}
	}
	return false;
}

bool	Map::checkCollisionEnemyAnim(glm::vec3 const &anim_l, float offset ){
	for (size_t i = 0; i < this->m_enemies.size(); i++ ) {
		if (abs(displayEntitiesController->getMovable(this->m_enemies.at(i)->m_enemyModelIndex)->getLocation().x - anim_l.x) < offset ) {
			if (abs(displayEntitiesController->getMovable(this->m_enemies.at(i)->m_enemyModelIndex)->getLocation().y - anim_l.y) < offset ) {
				if (abs(displayEntitiesController->getMovable(this->m_enemies.at(i)->m_enemyModelIndex)->getLocation().z - anim_l.z) < offset ) {
					return true;
				}
			}
		}
	}
	return false;
}

bool	Map::checkCollisionPlayerAnim( glm::vec3 const & anim_l, float offset ) {
	if (abs(displayEntitiesController->getMovable(m_player->playerIndex)->getLocation().x - anim_l.x) < offset ) {
		if (abs(displayEntitiesController->getMovable(m_player->playerIndex)->getLocation().y - anim_l.y) < offset ) {
			if (abs(displayEntitiesController->getMovable(m_player->playerIndex)->getLocation().z - anim_l.z) < offset ) {
				return true;
			}
		}
	}
	return false;
}

bool	Map::checkCollisionWalls( Location const & l ) {
	for (size_t i = 0; i < this->m_walls.size(); i++ ){
		if (this->m_walls.at(i)->location == l) {
			return true;
		}
	}
	return false;
}

bool	Map::checkCollisionPlayer( Location const & l ) {
	return this->m_player->m_location == l;
}

bool	Map::checkCollisionBombs( Location const & l ) {
	for (size_t i = 0; i < this->m_bombs.size(); i++ ){
		if (this->m_bombs.at(i)->getLocation() == l) {
			return true;
		}
	}
	return false;
}

bool	Map::checkCollisionPowerup( Location const & l ) {
	for (size_t i = 0; i < this->m_powerups.size(); i++ ){
		if (this->m_powerups.at(i)->location == l) {
			return true;
		}
	}
	return false;
}

void	Map::spawnBombTrail(unsigned int range, Location origin, int axis, int dir) {
	Location l;

	for (unsigned int rIndex = 0; rIndex < range; rIndex++) {
		l = origin;
		if (dir > 0) {
			l.append(((rIndex + 1) * axis), 0);
		} else {
			l.append(0, ((rIndex + 1) * axis));
		}
		if (this->checkCollisionWalls(l)) {
			rIndex = range;
		}
		this->spawnBomb(l, 0.3, 0);
	}
}

void	Map::tryDestroyEnemy() {
	Enemy		*enemy;
	Bomb		*bomb;

	for (size_t enemyIndex = 0; enemyIndex < m_enemies.size() ; enemyIndex++ ) {
		enemy = m_enemies.at(enemyIndex);

		for (size_t i = 0; i < m_bombs.size(); i++) {
			bomb = m_bombs.at(i);

			if (bomb->getRange() != 0) {
				continue ;
			}

			if (glmLocationEq(displayEntitiesController->getMovable(enemy->m_enemyModelIndex)->getLocation(), displayEntitiesController->getnonMovable(bomb->bombIndex)->getLocation())) {\
				destroyEnemy(enemyIndex);
				m_player->increaseScore(100);
				break ;
			}
		}
	}
}

void	Map::tryDestroyWall( ) {
	Bomb		*bomb;
	Wall		*wall;

	for ( size_t wallIndex = 0 ;  wallIndex < m_walls.size() ; wallIndex++ ) {
		wall = m_walls.at(wallIndex);

		if (wall->type == E_INVULNERABLE) {
			continue ;
		}

		for (size_t i = 0; i < m_bombs.size(); i++) {
			bomb = m_bombs.at(i);

			if (bomb->getRange() != 0) {
				continue ;
			}

			if (wall->location == bomb->getLocation()) {
				if (wall->type == E_HIDDENDOORWAY) {
					spawnPowerup(wall->location, E_DoorClosed);
				} else if (rand() % 100 > 80) {
					if (rand() % 2) {
						spawnPowerup(wall->location, E_FireUp);
					} else {
						spawnPowerup(wall->location, E_OneUp);
					}
				}
				destroyWall(wallIndex);
				m_player->increaseScore(50);
				break ;
			}
		}
	}
}

void	Map::tryDamagePlayer( ) {
	Bomb	*bomb;

	for (size_t i = 0; i < m_bombs.size(); i++) {
		bomb = m_bombs.at(i);

		if (bomb->getRange() != 0) {
			continue ;
		}
		if (checkCollisionPlayerAnim(displayEntitiesController->getnonMovable(bomb->bombIndex)->getLocation())) {
			this->m_player->takeDamage();
		}
	}
}

void	Map::tickBombs() {
	for (size_t i = 0; i < m_bombs.size(); i++) {
		m_bombs.at(i)->tick();
	}
}

void	Map::explodeExpiredBombs() {
	for (size_t i = 0; i < m_bombs.size(); i++) {
		Bomb *bomb = this->m_bombs.at(i);

		if (!bomb->checkActive()) {
			if (bomb->getRange() > 0) {
				spawnBomb(bomb->getLocation(), 0.3, 0);
				spawnBombTrail(bomb->getRange(), bomb->getLocation(), -1, 1);
				spawnBombTrail(bomb->getRange(), bomb->getLocation(), -1, -1);
				spawnBombTrail(bomb->getRange(), bomb->getLocation(), 1, -1);
				spawnBombTrail(bomb->getRange(), bomb->getLocation(), 1, 1);
			}
			if (!bomb->functionToCheckIfTheBombIsStillExploding()) {
			    destroyBomb(i);
			    if (G_VOLUME > 0)
				    this->m_bomb_explode_sound.changeVolume(G_VOLUME + 10);
				else
                    this->m_bomb_explode_sound.changeVolume(G_VOLUME);
			    this->m_bomb_explode_sound.playSound();
			}
		}
	}
}

void	Map::handleBombs() {
    tickBombs();
	explodeExpiredBombs();
}

void	Map::tryMoveEnemies() {
	Location	dst;
	Enemy		*enemy;


	for (size_t i = 0; i < this->m_enemies.size(); i++ ){
		enemy = m_enemies.at(i);

		if ((enemy->m_location == enemy->m_dst) || (enemy->m_previousLoc == enemy->m_location)){
			enemy->m_dst = getRandomDst();
		}

		if ((dst = enemy->move()) == enemy->m_location) {
			return ;
		}
		enemy->m_previousLoc = enemy->m_location;
		if (!checkCollisionWalls(dst)) {
			if (!checkCollisionBombs(dst)) {
				if (!checkCollisionEnemy(dst)) {
					if (std::fabs(displayEntitiesController->getMovable(enemy->m_enemyModelIndex)->getLocation().x - enemy->m_location.x * 2) < 1) {
						if (std::fabs(displayEntitiesController->getMovable(enemy->m_enemyModelIndex)->getLocation().z - enemy->m_location.y * 2) < 1) {
							enemy->m_location = dst;
						}
					}
				}
			}
		}
	}
}

void	Map::handleEnemies() {
	tryMoveEnemies();
	tryDestroyEnemy();
}

void	Map::handleWalls( void ) {
	tryDestroyWall();
	for (auto &wall : m_walls) {
		if (displayEntitiesController->getnonMovable(wall->m_modelIndex)->getLocation().y > 1 ) {
			displayEntitiesController->getnonMovable(wall->m_modelIndex)->playAnim("DIE");
		}
	}
}

void	Map::handlePlayer( void ) {
	tryDamagePlayer();
	if (checkCollisionEnemyAnim(displayEntitiesController->getMovable(m_player->playerIndex)->getLocation())) {
	    m_player->takeDamage();
	}
}

void	Map::handlePowerups( void ) {
	for (size_t i = 0; i < this->m_powerups.size(); i++ ){

		if (this->m_powerups.at(i)->getType() != E_DoorClosed && this->m_powerups.at(i)->getType() != E_DoorOpen) {
			if (!displayEntitiesController->getnonMovable(this->m_powerups.at(i)->powerUpIndex)->isAnimationActive()){
				displayEntitiesController->getnonMovable(this->m_powerups.at(i)->powerUpIndex)->playAnim("rotate");
			}
		}


		if (this->m_powerups.at(i)->getType() == E_DoorClosed) {
			if (this->m_enemies.empty()) {
				spawnPowerup(this->m_powerups.at(i)->location, E_DoorOpen);
				destroyPowerup(i);
			}
		}

		if (this->m_powerups.at(i)->location == this->m_player->m_location) {
			switch(this->m_powerups.at(i)->getType()) {
				case(E_None) : {
					break ;
				}
				case(E_FireUp) : {
				    if (G_VOLUME > 0)
					    this->m_fire_range_increase_sound.changeVolume(G_VOLUME + 20);
					else
                        this->m_fire_range_increase_sound.changeVolume(G_VOLUME );
				    this->m_fire_range_increase_sound.playSound();
					this->m_player->increaseBombRadius();
					break ;
				}
				case(E_OneUp) : {
				    if (G_VOLUME > 0)
					    this->m_health_increase_sound.changeVolume(G_VOLUME + 75);
					else
                        this->m_health_increase_sound.changeVolume(G_VOLUME);
				    this->m_health_increase_sound.playSound();
					this->m_player->recoverLives();
					break ;
				}
				case (E_DoorClosed) : {
					std::cout << "You need to destroy all enemies to open door!\n" << this->m_enemies.size() << " remaining!\n";
					return ;
				}
				case (E_DoorOpen) : {
					std::cout << "NEXT LEVEL\n";
					this->m_nextLevel = true;
					break ;
				}
			}
			destroyPowerup(i);
		}
	}
}

Location	Map::getRandomDst( unsigned int range ) {
	Location	dst;
	int			tries = 0;
	int			n = range;

	do {
		dst.x = (range == 0) ? rand() % this->m_worldWidth : (((rand() % 2) * 2) - 1) * n;
		dst.y = (range == 0) ? rand() % this->m_worldHeight : (((rand() % 2) * 2) - 1) * n;
		if (++tries == 50) {
			return dst;
		}
	} while (checkCollisionWalls(dst));
	return dst;
}

void		Map::increaseEnemyDifficulty( void ) {
	for (size_t i = 0; i < this->m_enemies.size(); i++ ){
		m_enemies.at(i)->setMoveSpeed(0);
	}
}

void		Map::addHiddenDoor( void ) {
	Location l;
	
	do {
	 	l.x = rand() % this->m_worldWidth;
	 	l.y = rand() % this->m_worldHeight;
	} while (checkCollisionWalls(l) || ((std::fabs(m_player->m_location.x - l.x) < 2 ) || (std::fabs(m_player->m_location.y - l.y) < 2)));

	spawnWall(l, E_HIDDENDOORWAY);
}

void		Map::generateWorld() {
	Location l;

	if (G_VOLUME > 0)
		this->m_next_level_sound.changeVolume(G_VOLUME + 10);
	else
		this->m_next_level_sound.changeVolume(G_VOLUME);
	this->m_next_level_sound.playSound();

	this->m_player->m_location = Location(1, 1);
	for (Uint32 y = 0; y < m_worldHeight; y++ ) {
		for (Uint32 x = 0; x < m_worldWidth; x++ ) {
			l = Location(x, y);

			if (((x == 0) || (x == m_worldWidth - 1)) || ((y == 0) || (y == m_worldHeight - 1)) || ((x % 2 == 0) && (y % 2 == 0))) {
				spawnWall(l, E_INVULNERABLE);
			}

			if (rand() % SEED == 0) {
				if (!checkCollisionWalls(l)) {
					if (!checkCollisionEnemy(l)) {
						if (!checkCollisionPlayer(l)) {
							if ((std::fabs(m_player->m_location.x - l.x) < 2 ) && (std::fabs(m_player->m_location.y - l.y) < 2)) {
								continue ;
							}

							if ((int)this->m_enemies.size() < 6 * this->m_level && rand() % SEED == 0) {
								spawnEnemy(l);
							} else {
								spawnWall(l, E_DESTRUCTIBLE);
							}
						}
					}
				}
			}
		}
	}
}

bool		Map::getNextLevelFlag( void ) {
	return m_nextLevel;
}

std::vector<Wall *>		Map::getWalls( void ) {
	return this->m_walls;
}

std::vector<Bomb *>		Map::getBombs( void ) {
	return this->m_bombs;
}

std::vector<Enemy *>	Map::getEnemies( void ) {
	return this->m_enemies;
}

std::vector<Powerup *>	Map::getPowerups( void ) {
	return this->m_powerups;
}

void 		Map::destroyWall(int index) {
	displayEntitiesController->destroyNonMovable(this->m_walls.at(index)->m_modelIndex);
	destroyElem(this->m_walls, index);
}

void 		Map::destroyBomb(int index) {
	displayEntitiesController->destroyNonMovable(this->m_bombs.at(index)->bombIndex);
	destroyElem(m_bombs, index);
}

void 		Map::destroyEnemy(int index) {
	displayEntitiesController->destroyMovable(this->m_enemies.at(index)->m_enemyModelIndex);
	destroyElem(m_enemies, index);
}

void 		Map::destroyPowerup(int index) {
	displayEntitiesController->destroyNonMovable(this->m_powerups.at(index)->powerUpIndex);
	destroyElem(m_powerups, index);
}