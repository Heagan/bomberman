#include "Game.class.hpp"
#include "Level.hpp"


Game::Game( void ) : m_theme("assets/music/boss.wav"), m_bounce("assets/sound/bounce.wav"), m_victory_sound("assets/sound/winning.wav"), m_lose_sound("assets/sound/lose.wav"){
	this->m_map = NULL;
	this->m_player = Player::getInstance();
	this->m_controls.setKeyToValue(E_Up, SDLK_UP);
	this->m_controls.setKeyToValue(E_Down, SDLK_DOWN);
	this->m_controls.setKeyToValue(E_Left, SDLK_LEFT);
	this->m_controls.setKeyToValue(E_Right, SDLK_RIGHT);
	this->m_controls.setKeyToValue(E_DropBomb, SDLK_SPACE);
	this->m_start = true;
	this->m_level = 1;
}

Game::Game( Game &object ) {
	*this = object;
}

Game::~Game( void ) {

}

Game	&Game::operator=( Game const &object ) {
	this->m_map = object.m_map;
	return *this;
}

void	Game::startGame() {
	m_gameWindow = new Window(true, 4);
	m_renderer = new Renderer(m_gameWindow->getWindowPtr());
	m_gameWindow->registerObserver(m_renderer);

	m_gui = new GUI(*this->m_gameWindow, this->m_renderer->getGlContext());

    m_theme.changeVolume(G_VOLUME - 20);
	m_theme.playMusic();
	m_gui = new GUI(*this->m_gameWindow, this->m_renderer->getGlContext())   ;
	this->m_renderer->getShaderProgram().bind();
	m_modelDispenser = new ModelDispenser();
	displayEntitiesController = new animation::DisplayEntitiesController(m_modelDispenser, &m_renderer->getShaderProgram());

	loadMap();
	m_player->playerIndex = displayEntitiesController->createMovable(m_player->m_location.x * 2, 0, m_player->m_location.y * 2, "movables/green_slime.obj");
	initCamera();

	if (!m_gameWindow) {
		puts("ERROR GAME WINDOW");
		exit(-1);
	}

	m_inGamePlayTextDisplay = new InGamePlayTextDisplay(m_gameWindow->getCurrentResolution()[0], m_gameWindow->getCurrentResolution()[1], "assets/Heavy/heavy_data.ttf", 32);
	m_inGamePlayTextDisplay->resetUpdatableValues();

	this->m_pausedTime = 0;
	this->m_unpausedTime = 0;
	gameLoop();
}

void	Game::gameLoop() {
	static bool loadedData = false;

	this->m_running = true;
	while (m_running) {
		//this->m_camera->animatedMove(0.2, m_camera->getMinTranslation(), glm::vec3(-50, 7, 0));
		if (this->m_gui->wasStartNewGamePressed()) {
			this->m_levelStartTime = SDL_GetTicks();
			this->m_totalPauseDuration = 0;
			this->m_pausedTime = 0;
			this->m_unpausedTime = 0;
		}

		if (G_VOLUME > 0)
        	m_theme.changeVolume(G_VOLUME - 20);
		else
			m_theme.changeVolume(0);
		if (m_gui->shouldLoad() && !loadedData) {
			loadSaveData();
			loadMap();
		}
		this->m_map->checkVolume();
		keyManger();
		if (!this->m_gui->isPaused() && this->m_gui->hasStarted()) {
			update();
			m_theme.stopMusic();
			m_map->playMusic();
		}
		render();
		checkPlayer();

		if (m_gui->shouldSave()) {
			saveData();
		}

		if (this->m_gui->hasEnded())
			this->m_running = false;

		if (this->m_gui->shouldQuitToMainMenu()) {
		    m_level = 1;
		    m_player->reinit();
		    bool isDebugOn = this->m_gui->getDebugStatus();
		    delete m_gui;
            this->m_gui = new GUI(*this->m_gameWindow, this->m_renderer->getGlContext());
            this->m_gui->setDebugStatus(isDebugOn);
            this->m_theme.playMusic();
            loadMap();
		}
	}
	delete displayEntitiesController;
	this->m_gui->clean();
}

void	Game::update( void ) {
	updateTimeRemaining();
	this->m_map->handleWalls();
	this->m_map->handleEnemies();
	this->m_map->handlePlayer();
	this->m_map->handlePowerups();
	this->m_map->handleBombs();
	handleMapProgress();

	updateAnimation(m_player->m_location, m_player->playerIndex);
	for (auto &e : m_map->getEnemies()) {
		updateAnimation(e->m_location, e->m_enemyModelIndex);
	}
}

void	Game::render( void ) {

    this->m_gui->createFrame();
	this->m_gui->debugMenu();
    this->m_gui->gameMenu(this->m_gameWindow->getCurrentResolution()[0], this->m_gameWindow->getCurrentResolution()[1]);
	this->m_renderer->getShaderProgram().bind();
	m_cameraTrans.x = displayEntitiesController->getMovable(m_player->playerIndex)->getLocation().x;
	m_camera->update();
	m_camera->translate(m_cameraTrans);

	if (this->m_gui->hasStarted()) {
		displayEntitiesController->display();
		drawGuiText();
	}
    this->m_gui->render();
	m_renderer->update();

}

void	Game::keyManger() {
	static E_Input changeT;
	int keyCode = -1;

	while (SDL_PollEvent(&event)) {
		if (event.type == SDL_KEYDOWN) {
			keyCode = event.key.keysym.sym;
		}
		if (event.type == SDL_QUIT)
			this->m_running = false;

		if (strlen(SDL_GetScancodeName(event.key.keysym.scancode)) < 1) {
			return ;
		}

		switch (m_gui->getChangedKey()) {
			case (E_DEFAULT) : {
				break ;
			}
			case (E_UP) : {
				changeT = E_Up;
				break ;
			}
			case (E_DOWN) : {
				changeT = E_Down;
				break ;
			}
			case (E_LEFT) : {
				changeT = E_Left;
				break ;
			}
			case (E_RIGHT) : {
				changeT = E_Right;
				break ;
			}
			case (E_BOMB) : {
				changeT = E_DropBomb;
				break ;
			}
		}

		if (keyCode != -1) {
			if (changeT) {
				this->m_controls.setKeyToValue(changeT, keyCode);
				changeT = E_Default;
				m_gui->resetChangedKey();
			}
		}

		if (keyCode == SDLK_ESCAPE || keyCode == SDLK_p) {
			if (this->m_gui->isPaused()) {
				this->m_gui->unpause();
				this->m_unpausedTime = SDL_GetTicks();
				this->m_totalPauseDuration += this->m_unpausedTime - this->m_pausedTime;
			} else {
				this->m_timeRemaining = this->getTimeRemaining();
				this->m_gui->pause();
				this->m_pausedTime = SDL_GetTicks();
			}
		}

		if (!this->m_gui->isPaused() && this->m_gui->hasStarted()) {
			if (keyCode == this->m_controls.getValueFromKey(E_Up))
				tryMovePlayer(0, -1);
			if (keyCode == this->m_controls.getValueFromKey(E_Down))
				tryMovePlayer(0, 1);
			if (keyCode == this->m_controls.getValueFromKey(E_Left))
				tryMovePlayer(-1, 0);
			if (keyCode == this->m_controls.getValueFromKey(E_Right))
				tryMovePlayer(1, 0);
			if (keyCode == this->m_controls.getValueFromKey(E_DropBomb)) {
			    glm::vec3 player3dloc = displayEntitiesController->getMovable(m_player->playerIndex)->getLocation();
			    Location player2dloc;

			    player2dloc.x = ceil(player3dloc.x / 2);
                player2dloc.y = ceil(player3dloc.z / 2);

                this->m_map->spawnBomb(player2dloc, 2, this->m_player->getBombRadius());
			}
		}
	}
}

void	Game::loadMap( void ) {
	if (this->m_map) {
		m_camera->animatedMove(0.2, m_camera->getMinTranslation(), glm::vec3(-50, 7, 0));
		displayEntitiesController->destroyMovable(m_player->playerIndex);
		m_player->playerIndex = displayEntitiesController->createMovable(2, 0, 2, "movables/green_slime.obj");
		delete this->m_map;
		this->m_inGamePlayTextDisplay->clearText();
		this->m_inGamePlayTextDisplay->resetUpdatableValues();

		this->m_levelStartTime = SDL_GetTicks();
		this->m_totalPauseDuration = 0;
		this->m_pausedTime = 0;
		this->m_unpausedTime = 0;

	}
	this->m_map = new Map(15, 15, this->displayEntitiesController, this->m_level);
	this->m_map->generateWorld();
	this->m_map->addHiddenDoor();
	if (this->m_start) {
		createWorldFloor();
		this->m_start = false;
	}
	this->m_levelStartTime = SDL_GetTicks();
}

unsigned long Game::getTimeRemaining() {
	int long currentTime = SDL_GetTicks();
	int long totalDuration = currentTime - this->m_levelStartTime;

	int long timeElapsedInGame = totalDuration - this->m_totalPauseDuration;

	int f = (m_gameCountdownInMilliseconds - timeElapsedInGame) / 1000;

	return (Uint32)(f < 0 ? 0 : f);
}

void	Game::updateTimeRemaining() {
	unsigned long	currentTime = SDL_GetTicks();
	unsigned long	totalDuration = currentTime - this->m_levelStartTime;
	unsigned long	timeElapsedInGame = totalDuration - this->m_totalPauseDuration;
	static bool		activated = false;

	if (this->m_gui->isPaused()) {
		if (this->m_gameCountdownInMilliseconds < timeElapsedInGame && !activated) {
			this->m_map->increaseEnemyDifficulty();
			activated = true;
			std::cout << "Enemies difficuty inceased!\nThey are now faster!\n";
		} else {
			activated = false;
		}
	}
}

void	Game::handleMapProgress( void ) {
	if (this->m_map->getNextLevelFlag()) {
		this->m_level++;
		loadMap();
	}
	if (this->m_level > 3) {
        displayWinGame();
	}
}

void	Game::drawGuiText() {
	this->m_inGamePlayTextDisplay->clearText();
	if (this->m_gui->isPaused()) {
		this->m_inGamePlayTextDisplay->setCurrentTime(this->m_timeRemaining);
	} else {
		this->m_inGamePlayTextDisplay->setCurrentTime(getTimeRemaining());
	}
	this->m_inGamePlayTextDisplay->resetUpdatableValues();
	this->m_inGamePlayTextDisplay->addNewText("Lives: ", {25, 50, 0}, {1, 0.6, 0.7});
	this->m_inGamePlayTextDisplay->addNewText("Range: ", {275, 50, 0}, {1, 0, 0});
	this->m_inGamePlayTextDisplay->addNewText("Score: ", {525, 50, 0}, {0, 0, 1});
	this->m_inGamePlayTextDisplay->addNewText("Time: ", {900, 50, 0}, {0, 1, 0});
	this->m_inGamePlayTextDisplay->update();
}

void	Game::updateAnimation(Location l, int index) {
	if (displayEntitiesController->getMovable(index)->getLocation().x <= 0)
		return ;

	if (std::fabs(displayEntitiesController->getMovable(index)->getLocation().x - l.x * 2) > 1.5) {
		if (displayEntitiesController->getMovable(index)->getLocation().x < l.x * 2) {
			displayEntitiesController->getMovable(index)->moveRight();
		} else {
			displayEntitiesController->getMovable(index)->moveLeft();
		}
	}
	if (std::fabs(displayEntitiesController->getMovable(index)->getLocation().z - l.y * 2) > 1.5) {
		if (displayEntitiesController->getMovable(index)->getLocation().z < l.y * 2) {
			displayEntitiesController->getMovable(index)->moveUp();
		} else {
			displayEntitiesController->getMovable(index)->moveDown();
		}
	}
}

void 	Game::initCamera() {
	this->m_camera = new Camera(&m_renderer->getShaderProgram(), 30 * 1.7, 30);
	glm::vec3 cameraRotation(-50, 7, 0);
	m_camera->rotate(cameraRotation);

	m_cameraTrans = glm::vec3(m_camera->getWidthAndHeight().x / 2 - 5, 0.0f, 10.0f);
	m_cameraTrans.y -= 13;
	m_cameraTrans.z += 35;
	this->m_camera->translate(m_cameraTrans);
	//m_camera->setTranslationLimits({m_camera->getWidthAndHeight().x / 2 - 5, m_cameraTrans.y, m_cameraTrans.z},
	//							{((float)this->m_map->m_worldWidth * 2.0f) - (m_camera->getWidthAndHeight().x / 2.0f), m_cameraTrans.y, m_cameraTrans.z});
}

void 	Game::checkPlayer() {
	static int previous_life = m_player->getLives();

	if (previous_life > m_player->getLives()) {
		SDL_Delay(1000);
		previous_life = m_player->getLives();
		loadMap();
	}
	if (m_player->getLives() <= 0) {
		std::cout << "YOUR PLAYER RAN OUT OF LIVES!!!\nGAME OVER!!!\n";
		displayGameOver();
		m_player->recoverLives(3);
	}
	previous_life = m_player->getLives();
}

void	Game::tryMovePlayer(int x, int y) {
	Location collisionLocation;

	collisionLocation = this->m_player->m_location;
	collisionLocation.append(x, y);
	if (m_map->checkCollisionWalls(collisionLocation) || m_map->checkCollisionBombs(collisionLocation )) {
		std::cout << "Cant move there..." << std::endl;
	} else {
		if (std::fabs(displayEntitiesController->getMovable(m_player->playerIndex)->getLocation().x - m_player->m_location.x * 2) < 1) {
			if (std::fabs(displayEntitiesController->getMovable(m_player->playerIndex)->getLocation().z - m_player->m_location.y * 2) < 1) {
				m_player->m_location.append(x, y);
				if (G_VOLUME > 0)
                    this->m_bounce.changeVolume(G_VOLUME + 20);
				else
                    this->m_bounce.changeVolume(G_VOLUME);
				this->m_bounce.playSound();
			}
		}
	}
}

void 	Game::createWorldFloor( void ) {
	displayEntitiesController->createNonMovable(10, -2, -10, "non_movables/background.obj");
	for (unsigned y = 0; y < this->m_map->m_worldHeight; y++) {
		for (unsigned x = 0; x < this->m_map->m_worldWidth ; x++) {
			displayEntitiesController->createNonMovable(x * 2, -2, y * 2, "non_movables/grass.obj");
		}
	}
}

void 	Game::displayGameOver() {
	this->m_victory_sound.changeVolume(G_VOLUME);
	this->m_victory_sound.playSound();
	m_inGamePlayTextDisplay->clearText();
	m_inGamePlayTextDisplay->addNewText("GAME OVER", { 50, 50, 0}, {1, 0, 0});
	m_inGamePlayTextDisplay->update();
	m_renderer->update();
	SDL_Delay(5000);
	m_level = 1;
	m_player->reinit();
	delete m_gui;
	this->m_gui = new GUI(*this->m_gameWindow, this->m_renderer->getGlContext());
}

void 	Game::displayWinGame() {
	this->m_victory_sound.changeVolume(G_VOLUME);
	this->m_victory_sound.playSound();
	m_inGamePlayTextDisplay->clearText();
	m_inGamePlayTextDisplay->addNewText("YOU HAVE WON THE GAME!", { 50, 50, 0}, {0, 0, 1});


	std::string score = "YOUR SCORE: ";
	score +=  std::to_string(m_player->getScore());

	m_inGamePlayTextDisplay->addNewText(score, { 50, 150, 0}, {0, 0, 1});
	m_inGamePlayTextDisplay->update();
	m_renderer->update();
	SDL_Delay(5000);
	m_level = 1;
	m_player->reinit();
	if (m_gui) {
	    delete m_gui;
	}
	this->m_gui = new GUI(*this->m_gameWindow, this->m_renderer->getGlContext());
}

void	Game::saveData() {
	std::ofstream	file("savedata.sav");
	file << this->m_level << "\n" << m_player->getScore() << "\n";
	file << this->m_player->getLives() << "\n" << m_player->getBombRadius() << "\n";
	file << m_gameWindow->getCurrentResolution()[0] << "," << m_gameWindow->getCurrentResolution()[1]  << std::endl;
	file.close();
}

void	Game::loadSaveData() {
	std::ifstream	file("savedata.sav");	
	std::string 	line;

	if (!file) {
		std::cout << "Couldn't open file" << std::endl;
		return ;
	}

	std::getline(file, line);
	std::string level	= line;
	this->m_level		= atoi(level.c_str());

	std::getline(file, line);
	this->m_player->increaseScore(std::abs(atoi(line.c_str())));

	std::getline(file, line);
	this->m_player->setLives(atoi(line.c_str()));

	std::getline(file, line);
	this->m_player->increaseBombRadius(std::abs(atoi(line.c_str())) - m_player->getBombRadius());

	int r[2];
	std::getline(file, line, ',');
	r[0] = atoi(line.c_str());
	std::getline(file, line, ',');
	r[1] = atoi(line.c_str());

	for (unsigned int i = 0; i < m_gameWindow->getValidResolutionList().size(); ++i) {
		if ((m_gameWindow->getValidResolutionList()[i][0] == r[0]) && (m_gameWindow->getValidResolutionList()[i][1] == r[1])) {
			m_gameWindow->setResolution(i);
		}
	}
	std::cout << "Loaded Save File:" << " - level: " << this->m_level << std::endl;
}
