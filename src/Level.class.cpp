#include "Level.hpp"

Level::Level( void ) {

}

Level::Level(char *name)
{
	this->lines = 0;
	this->file.open(name, std::ifstream::in);
	this->buf << file.rdbuf();
	std ::string fileContents = buf.str();
	this->fileContents = buf.str();
	this->lines = std::count(fileContents.begin(), fileContents.end(), '\n');
}

Map	*Level::createWorld()
{
    Map *ret		= new Map(0, 0, NULL);
    size_t actual	= 0;
	int j			= 0;

    for (size_t i = 0; i < fileContents.length(); i++)
    {
		if (actual > fileContents.length())
			break;
		if (fileContents[actual] == '\n') {
			j++;
			i = -1;
		}
       	Location tmp(i, j,0);
        this->parseChar(fileContents[actual], ret, tmp);
        actual++;
    }
	std::cout << "Map dimensions X: " << fileContents.length() / (j + 1) << " Y: " << j + 1 << std::endl;
	ret->m_worldWidth = fileContents.length() / (j + 1);
	ret->m_worldHeight = j + 1;

    return ret;
}

void	Level::parseChar(char c, Map *World, Location l)
{
    switch (c)
    {
        case 'B':
        case 'b':
            World->spawnBomb(l);
            break;
		case '1':
            World->spawnWall(l);
            break;
		case 'X':
            World->spawnWall(l, E_DESTRUCTIBLE);
            break;
		case 'E':
            World->spawnEnemy(l);
            break;
		case 'P':
            Player::getInstance()->m_location = l;
            break;
        default:
            break;
    }
}

Level::~Level() {

}

