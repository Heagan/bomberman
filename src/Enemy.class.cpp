#include "Enemy.class.hpp"

Enemy::Enemy( Location l ) {
	this->m_location = l;
	this->m_dst = l;
	this->m_previousLoc = l;
	this->m_health = 100;
	this->m_timerInMilliseconds = 0;
	this->m_moveSpeed = 500;
}

Enemy::Enemy( Enemy &object ) {
	*this = object;
}

Enemy::~Enemy() {

}

Enemy	&Enemy::operator=( Enemy const &object ) {
	(void)object;
	return *this;
}

bool		Enemy::delay() {
	if (SDL_GetTicks() - m_timerInMilliseconds < m_moveSpeed) {
		return false;
	}
	m_timerInMilliseconds = SDL_GetTicks();
	return true;
}

Location	Enemy::move() {
	Location l = this->m_location;

	if (this->delay()) {
		if (rand() % 2) {
			if (m_dst.x != m_location.x) {
				l.x += ((m_dst.x > m_location.x) * 2) - 1;
			} 
		} else if (m_dst.y != m_location.y) {
			l.y += ((m_dst.y > m_location.y) * 2) - 1;
		} 
	}
	return l;
}

void		Enemy::setMoveSpeed( unsigned int moveSpeed ) {
	this->m_moveSpeed = moveSpeed;
}
