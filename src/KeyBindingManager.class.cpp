#include "KeyBindingManager.class.hpp"

KeyBindingManager::KeyBindingManager( void ) {

}

KeyBindingManager::KeyBindingManager( KeyBindingManager &object ) {
	*this = object;
}

KeyBindingManager::~KeyBindingManager( void ) {

}

KeyBindingManager	&KeyBindingManager::operator=( KeyBindingManager const &object ) {
	this->m_keys = object.m_keys;
	return *this;
}

int		KeyBindingManager::getValueFromKey(E_Input key ) {
	return this->m_keys[key];
}
void	KeyBindingManager::setKeyToValue(E_Input key, int keyCode) {
	this->m_keys[key] = keyCode;
}