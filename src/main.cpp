#include <GenericProgException.hpp>
#include "bomber.h"
#include "Game.class.hpp"

int G_VOLUME = 50;

int		main( int ac, char **av ) {
	
	Game g;

	srand(SEED);

	(void)ac;
	(void)av;

	try {
        g.startGame();
    }
	catch(GenericProgException &e) {
        std::cout << "Invalid usage:: " << e.what() << std::endl;
	}

	return 0;
}