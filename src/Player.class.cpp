#include "Player.class.hpp"

Player *Player::player = nullptr;

Player::Player( int lives, int score ) : _lives(lives), _score(score), _bombRadius(1) , m_die_sound("assets/sound/hurt_arg.wav")  {

}

Player::Player( Player &object ) {
	*this = object;
}

Player::~Player( void ) {

}

Player	&Player::operator=( Player const &object ) {
	this->_lives = object._lives;
	this->_score = object._score;
	return *this;
}

Player	*Player::getInstance( void ) {
	if (player == nullptr) {
		player = new Player();
	}

	return player;
}

void	Player::takeDamage( int damage ) {
	if (SDL_GetTicks() - this->_damageDelayInMilliseconds > 2000 ) {
		_lives -= damage;
		this->_damageDelayInMilliseconds = SDL_GetTicks();
		std::cout << "Player took " << damage << " damage...\n" << _lives << " remaining life left\n";
        if (G_VOLUME > 0)
		    this->m_die_sound.changeVolume(G_VOLUME + 50);
        else
            this->m_die_sound.changeVolume(G_VOLUME);
        this->m_die_sound.playSound();
	}
}
void	Player::recoverLives( int amount ) {
	_lives += amount;
	std::cout << "Player gained " << amount << " life!!!\n" << _lives << " remaining life left\n";
}

void	Player::increaseBombRadius( unsigned int amount ) {
	this->_bombRadius += amount;
	std::cout << "Bomb radius increased!\nBomb radius: " << this->_bombRadius << std::endl;
}

void 	Player::increaseScore(unsigned int amount) {
	this->_score += amount;
}

void 	Player::reinit() {
	this->_bombRadius = 1;
	this->_score = 0;
	this->_lives = 3;
}

void 	Player::setLives(int amount) {
	this->_lives = amount;
}