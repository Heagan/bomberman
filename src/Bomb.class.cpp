#include "Bomb.class.hpp"

Bomb::Bomb( Location location , float countdownInSeconds, Uint32 range, float explosionDurationInSeconds) : m_location(location), m_range(range), m_active(true),
																										 m_countdownInMilliseconds(countdownInSeconds * 1000),
																										 m_explosionDurationInMilliseconds(explosionDurationInSeconds * 1000)
{
	m_spawnTime = SDL_GetTicks();
}

Bomb::Bomb( Bomb &object ) {
	*this = object;
}

Bomb::~Bomb() {

}

Bomb	&Bomb::operator=( Bomb const &object ) {
	this->m_location = object.m_location;
	this->m_countdownInMilliseconds = object.m_countdownInMilliseconds;
	this->m_range = object.m_range;
	return *this;
}

bool 		Bomb::checkActive() {
	return this->m_active;
}

void	Bomb::explode() {
	this->m_active = false;
}

void	Bomb::tick() {
	if (SDL_GetTicks() - m_spawnTime >= m_countdownInMilliseconds) {
		explode();
	}
}

Location	&Bomb::getLocation() {
	return this->m_location;
}

unsigned int	Bomb::getRange() {
	return this->m_range;
}

bool		Bomb::functionToCheckIfTheBombIsStillExploding() {
	return SDL_GetTicks() - m_spawnTime < m_countdownInMilliseconds + m_explosionDurationInMilliseconds;
}
