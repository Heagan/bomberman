#include "Location.class.hpp"

Location::Location( int x, int y, int z ) : x(x), y(y), z(z) {

}

Location::Location( Location &object ) {
	*this = object;
}

Location::~Location( void ) {

}

Location	&Location::operator=( Location const &object ) {
	this->x = object.x;
	this->y = object.y;
	this->z = object.z;
	return *this;
}

Location	Location::operator-( Location const &object ) {
	Location	l;
	l.x = this->x - object.x;
	l.y = this->y - object.y;
	l.z = this->z - object.z;
	return l;
}

bool		Location::operator==( Location const &object ) {
	if ((this->x == object.x) && (this->y == object.y) && (this->z == object.z)) {
		return true;
	}
	return false;
}

Location	&Location::append(int x, int y) {
	this->x += x;
	this->y += y;
	return *this;
}
