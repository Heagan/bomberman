#include "Wall.class.hpp"

Wall::Wall( Location location, E_WallType type ) : type(type), location(location) {

}

Wall::Wall( Wall &object ) {
	*this = object;
}

Wall::~Wall( void ) {

}

Wall	&Wall::operator=( Wall const &object ) {
	(void)object;
	return *this;
}
