#include "Powerup.class.hpp"

Powerup::Powerup( Location location, E_Powerup type ) : location(location), type(type) {

}

Powerup::Powerup( Powerup &object ) {
	*this = object;
}

Powerup::~Powerup( void ) {

}

Powerup	&Powerup::operator=( Powerup const &object ) {
	this->location = object.location;
	this->type = object.type;
	return *this;
}

