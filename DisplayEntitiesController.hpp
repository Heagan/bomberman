#ifndef DISPLAY_ENT_CON_H
# define DISPLAY_ENT_CON_H

#include <SDL.h>
#include <iostream>
#include <gtc/matrix_transform.hpp>
#include <gtc/type_ptr.hpp>
#include "AssimpInterpreter.hpp"
#include "Model.hpp"
#include "Camera.hpp"
#include "Renderer.hpp"
#include "Window.hpp"
#include "ModelDispenser.hpp"
#include "Location.class.hpp"

#include <map>

class testNotification : public i_Observer {
public:
	bool m_isActive;

	testNotification() {
		this->m_isActive = false;
	}

	void notify(void *arg) override {
		static_cast<void>(arg);
		this->m_isActive = false;
	}
};

namespace animation {
    class modelWrapper {

	public :
		modelWrapper();
		modelWrapper(const modelWrapper&) = default;
		modelWrapper& operator= (const modelWrapper&) = default;
		modelWrapper(ModelDispenser *modelDispenser, Shader	*shaderProgram, std::string fname, std::string path);

		virtual ~modelWrapper();

        glm::vec3		getLocation();
		void			update();
        Animation		makeAnim(std::string animeName, double dir, double rot, double axis);
		Animation		makeAnimBouncy(std::string animeName, double dir, double rot, double axis);
		Animation		makeAnimRotate(std::string animeName, double rot, double axis);
		Transformation	playAnim(std::string name);

    protected:
		ModelDispenser		*m_modelDispenser;
		Shader				*m_shaderProgram;
        Model				*m_character;
        std::string			m_file_name;
		testNotification	testNotificationClass;

    };

    class Movable : public modelWrapper {

    public:
		Movable();
		Movable(const Movable&) = default;
        Movable& operator = (const Movable&) = default;
		Movable(ModelDispenser *modelDispenser, Shader *shaderProgram, std :: string fname, std::string path, int x = 0, int y = 0, int z = 0);
        ~Movable();
        void moveLeft();
        void moveRight();
        void moveUp();
        void moveDown();
        void set_loc(int x, int y);

    private:
        Animation moveLeftAnim;
        Animation moveRightAnim;
        Animation moveUpAnim;
        Animation moveDownAnim;

    };

    class nonMovable : public modelWrapper, i_Observer {
    public:
        nonMovable();
		nonMovable(const nonMovable&) = default;
		nonMovable& operator = (const nonMovable&) = default;
        nonMovable(ModelDispenser *modelDispenser, Shader *shaderProgram, std :: string fname, std::string path, int x = 0, int y = 0, int z = 0);
        ~nonMovable();
        bool	isAnimationActive();
        void playAnimation(std :: string name);

    private:
    	Animation	makeExplodeAnim();
		Animation	makeGrowAnim();

		void notify(void *arg) override;

		bool m_isAnimationActive;

		std::map <std::string ,Animation> AnimeList;

    };

    class DisplayEntitiesController {

	public:
    	DisplayEntitiesController(ModelDispenser *modelDispenser, Shader *shaderProgram) : modelDispenser(modelDispenser), shaderProgram(shaderProgram) {

    	}
    	DisplayEntitiesController &operator = (DisplayEntitiesController &) = default;
		DisplayEntitiesController (DisplayEntitiesController &) = default;
		~DisplayEntitiesController() {
			for (auto &m : movables) {
				destroyMovable(m.first);
			}
			for (auto &m : nonMovables) {
				destroyNonMovable(m.first);
			}
		}

		int    	createMovable(int x, int y, int z, std::string model);
		int    	createNonMovable(int x, int y, int z, std::string model);

		animation:: Movable *getMovable(int index) {
			return movables[index];
		}

		animation:: nonMovable *getnonMovable(int index) {
			return nonMovables[index];
		}

		void 		display() {
			for (auto &m : movables) {
				m.second->update();
			}
			for (auto &m : nonMovables) {
				m.second->update();
			}
		}

		void 		destroyMovable(int index);
		void 		destroyNonMovable(int index);

	private:
		ModelDispenser	*modelDispenser;
		Shader			*shaderProgram;
		std::map<int, animation::Movable *>		movables;
		std::map<int, animation::nonMovable *>	nonMovables;

    };

}

#endif